#!/bin/bash

SCRIPT_PATH="${BASH_SOURCE[0]}"
if [ -h "${SCRIPT_PATH}" ]
then
  while [ -h "${SCRIPT_PATH}" ]
  do 
      SCRIPT_PATH=$(readlink "${SCRIPT_PATH}")
  done
fi
pushd . > /dev/null
DIR_SCRIPT=$(dirname "${SCRIPT_PATH}" )
cd "${DIR_SCRIPT}" > /dev/null || exit 1
SCRIPT_PATH=$(pwd);
popd  > /dev/null || exit 1

if [ -z "$JAVA_HOME" ]
then
    JAVA_HOME="$HOME/graalvm-ce-java11-21.1.0"
    if [ ! -d "$JAVA_HOME" ]
    then 
        JAVA_HOME="$HOME/graalvm-ce-java11-21.0.0.2"
        if [ ! -d "$JAVA_HOME" ]
        then 
            JAVA_HOME=/usr/lib/jvm/java-11-openjdk-amd64
        fi
    fi
fi
export JAVA_HOME
PATH="$JAVA_HOME/bin:$PATH"
export PATH

if [ -z "$M2_HOME" ]
then
    M2_HOME=~/Data/.m2/
    if [ ! -d "$M2_HOME" ]
    then
        M2_HOME=~/.m2
    fi
fi

if [ -z "$MAVEN_HOME" ]
then
    MAVEN_HOME=~/apache-maven-3.8.1
    if [ ! -d "$MAVEN_HOME" ]
    then 
        MAVEN_HOME=/home/jenkins/tools/hudson.tasks.Maven_MavenInstallation/maven_3.8.1
        if [ ! -d "$MAVEN_HOME" ]
        then 
            MAVEN_HOME=/var/lib/jenkins/tools/hudson.tasks.Maven_MavenInstallation/maven_3.8.1
            if [ ! -d "$MAVEN_HOME" ]
            then
                echo "Maven inconnu!"
                exit 1
            fi
        fi
    fi
fi
export MAVEN_HOME
PATH="$MAVEN_HOME/bin:$PATH"
export PATH

WORKSPACE=$SCRIPT_PATH
echo "WORKSPACE: $WORKSPACE"
echo "MAVEN_HOME: $MAVEN_HOME"
echo "M2_HOME: $M2_HOME"
echo "JAVA_HOME: $JAVA_HOME"
echo "PATH: $PATH"
java --version
#touch /tmp/.eoleci
pushd . > /dev/null
cd "${WORKSPACE}" > /dev/null || exit 1

echo "*---------------------------------------------------------------------------------------------------------------------"
echo "* mvn -e clean install"
mvn -e clean install 

echo "*---------------------------------------------------------------------------------------------------------------------"
echo "* mvn versions:display-plugin-updates"
mvn versions:display-plugin-updates

echo "*---------------------------------------------------------------------------------------------------------------------"
echo "* mvn dependency:tree"
mvn dependency:tree

echo "*---------------------------------------------------------------------------------------------------------------------"
#find . -newer /tmp/.eoleci

#mkdir target/so/
#for J in target/libs/*.jar
#do
#	fichier=$(basename $J .jar)
#	echo $fichier
#    jaotc --verbose --directory target/libs/   --output target/so/${fichier}.so --jar $J
#done
 
popd  > /dev/null || exit 1
