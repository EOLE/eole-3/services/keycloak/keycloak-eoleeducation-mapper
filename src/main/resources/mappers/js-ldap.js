/**
 * Available variables: 
 * user - the current user (UserModel)
 * realm - the current realm (RealmModel)
 * token - the current token (TokenModel)
 * userSession - the current userSession (UserSessionModel)
 * keycloakSession - the current keycloakSession (KeycloakSessionModel)
 */

 // Attention genere avec type : module dans package.json
load('/opt/jboss/keycloak/js-attr/js-modules/ldap-bundle.js');

 //TODO lire attribut de l'utilisateur fourni par keycloak
 console.log(isHuman);
 
 function testGenre(user){    
    isHuman(user)  
     .then(result=> {
         return result;
     })
     .catch(err=> {
         return err
     });
 }
 
 // TODO tester genre
 // si GENRE.HUMAN => best friend = chien
 // si GENRE.ROBOT => best friend = un robot n'as pas d'ami
 // si GENRE.mutant => best friend = professeur Xavier
 // token.setOtherClaims("bestFriend","");
 
 const humanAmy = 'Amy Wong+sn=Kroker';
 const humanConrad =  'Hermes Conrad';
 const robotRodrig = 'Bender Bending Rodríguez';
 const mutantLeela = 'Turanga Leela';
 
 testGenre(mutantLeela);